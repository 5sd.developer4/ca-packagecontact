<?php

// CA\PackageContact\src\routes\web.php

Route::group([
    'namespace' => 'CA\PackageContact\Http\Controllers',
    'middleware' => ['web']],
    function(){
        Route::get('contact', 'ContactFormController@index');
        Route::post('contact', 'ContactFormController@sendMail')->name('contact');
    }
);
